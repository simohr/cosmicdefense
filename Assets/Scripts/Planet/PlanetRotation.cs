﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotation : MonoBehaviour {

    public Transform planet;
	
	// Update is called once per frame
	void Update ()
    {
        planet.GetComponent<Rigidbody>().transform.Rotate(0.01f, 0.05f, 0);
	}
}
