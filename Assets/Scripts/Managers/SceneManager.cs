﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneState
{
    MainMenu,
    GameOver
}

public class MySceneManager : Singleton<MySceneManager>
{
    public SceneState sceneState = SceneState.MainMenu;
    public bool inMenu = true;

    public void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void LoadGame()
    {
        sceneState = SceneState.GameOver;
        SceneManager.LoadScene("Main");
    }
    
}
