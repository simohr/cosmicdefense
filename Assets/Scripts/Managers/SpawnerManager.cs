﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : Singleton<SpawnerManager> {

    public List<Spawner> groundSpawners;
    public List<FlyingSpawner> flyingSpawners;
    public GameStatsUI gameStats;
    public PowerUpsUI powerUpsUI;

    [HideInInspector]
    public float roundTimeDuration = 60f;
    [HideInInspector]
    public float roundTime;
    [HideInInspector]
    private float afterRoundTimeDelay = 15f;
    [HideInInspector]
    public float afterRoundTime;
    [HideInInspector]
    public int round = 1;
    [HideInInspector]
    public bool roundActive = false;

    private float spawnGroundEnemiesDelay = 3f;
    private float spawnFlyingEnemiesDelay = 15f;
    private float spawnGroundEnemiesTime;
    private float spawnFlyingEnemiesTime;

    private int numberOfGroundEnemies = 8;
    private int numberOfFlyingEnemies = 2;

    void UpdateUI()
    {
        gameStats.round.text = round.ToString();
        int newRoundTime = ((int)(roundTime - Time.time));
        if (newRoundTime <= 0)
        {
            newRoundTime = 0;
        }
        gameStats.roundTime.text = newRoundTime.ToString();

        int newAfterRoundTime = ((int)(afterRoundTime - Time.time));
        if (newAfterRoundTime <= 0)
        {
            newAfterRoundTime = 0;
        }
        gameStats.afterRoundTime.text = newAfterRoundTime.ToString();
    }

    void OnEnable()
    {
        round = 1;
        roundActive = true;
        spawnFlyingEnemiesTime = Time.time + Time.deltaTime + spawnFlyingEnemiesDelay;
        spawnGroundEnemiesTime = Time.time + Time.deltaTime + spawnGroundEnemiesDelay;
        roundTime = Time.time + Time.deltaTime + roundTimeDuration;

        switch (DifficultyManager.Instance.difficulty)
        {
            case Difficulty.Easy:
                numberOfFlyingEnemies = 2;
                numberOfGroundEnemies = 8;
                break;
            case Difficulty.Medium:
                numberOfFlyingEnemies = 3;
                numberOfGroundEnemies = 10;
                break;
            case Difficulty.Hard:
                numberOfFlyingEnemies = 4;
                numberOfGroundEnemies = 12;
                break;
        }

        for (int i = 0; i < groundSpawners.Count; i++)
        {
            groundSpawners[i].difficultyIncrease = 1f;
        }
        for (int i = 0; i < flyingSpawners.Count; i++)
        {
            flyingSpawners[i].difficultyIncrease = 1f;
        }

        powerUpsUI.InitializePowerUps();
    }

    void UpdateEnemies()
    {
        if (round % 2 == 0)
        {
            float difficultyIncrease = 0.6f;

            switch (DifficultyManager.Instance.difficulty)
            {
                case Difficulty.Easy:
                    difficultyIncrease = 0.6f;
                    break;
                case Difficulty.Medium:
                    difficultyIncrease = 0.7f;
                    break;
                case Difficulty.Hard:
                    difficultyIncrease = 0.8f;
                    break;
            }

            for (int i = 0; i < groundSpawners.Count; i++)
            {
                groundSpawners[i].difficultyIncrease = round * difficultyIncrease;
            }
            for (int i = 0; i < flyingSpawners.Count; i++)
            {
                flyingSpawners[i].difficultyIncrease = round * difficultyIncrease;
            }
        }
        else
        {
            numberOfFlyingEnemies += 1;
            numberOfGroundEnemies += 2;
        }
    }

    void SpawnGroundEnemies(int numberOfEnemies)
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            groundSpawners[i % groundSpawners.Count].SpawnEnemy();
        }
    }

    void SpawnFlyingEnemies(int numberOfEnemies)
    {
        StartCoroutine(SpawnFlyingEnemiesCoroutine(numberOfEnemies));
    }

    IEnumerator SpawnFlyingEnemiesCoroutine(int numberOfEnemies)
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            flyingSpawners[i % flyingSpawners.Count].SpawnEnemy();
            yield return new WaitForSeconds(1f);
        }
    }

    void Update ()
    {
        if (roundActive)
        {
            if (Time.time > spawnGroundEnemiesTime)
            {
                spawnGroundEnemiesTime = Time.time + Time.deltaTime + roundTimeDuration / 2;
                SpawnGroundEnemies(numberOfGroundEnemies);
            }

            if (Time.time > spawnFlyingEnemiesTime)
            {
                spawnFlyingEnemiesTime = Time.time + Time.deltaTime + roundTimeDuration / 2;
                SpawnFlyingEnemies(numberOfFlyingEnemies);
            }
        }

        if (Time.time > roundTime && roundActive)
        {
            roundActive = false;
            afterRoundTime = Time.time + Time.deltaTime + afterRoundTimeDelay;
            round++;
            UpdateEnemies();
            powerUpsUI.GiveRandomPowerUp();
        }

        if (Time.time > afterRoundTime && !roundActive)
        {
            roundActive = true;
            roundTime = Time.time + Time.deltaTime + roundTimeDuration;
            spawnFlyingEnemiesTime = Time.time + Time.deltaTime + spawnFlyingEnemiesDelay;
            spawnGroundEnemiesTime = Time.time + Time.deltaTime + spawnGroundEnemiesDelay;
        }

        UpdateUI();
    }
}
