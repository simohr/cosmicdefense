﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Difficulty
{
    Easy,
    Medium,
    Hard
}
public class DifficultyManager : Singleton<DifficultyManager>
{
    public float score = 0;
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
    public Difficulty difficulty = Difficulty.Easy;
}
