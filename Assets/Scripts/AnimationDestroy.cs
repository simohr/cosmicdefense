﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.Destroy(gameObject, 1f);
	}
	
}
