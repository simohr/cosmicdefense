﻿using UnityEngine;

public class Rhino : Enemy
{
    public bool isSpecial;
    public int indexLocation;
    public Rhino(float maxHP, float damage, float armor, int points, float speed) : base(maxHP, damage, armor, points, speed)
    {
        int random = Random.Range(1, 100);
        if (random > 100)
        {
            isSpecial = false;
        }
        else
        {
            isSpecial = true;
            indexLocation = Random.Range(0,4);
        }
    }

}
