﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhinoBehavior : EnemyBehavior {

    public List<GameObject> teleportLocations;
    public GameObject teleportAnimation;
    void Start()
    {
        base.Start();
    }

    void Update()
    {
        base.Update();
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("damage"))
        {
            animator.SetBool("Damaged", false);
        }
    }

    public override void TakeDamage(float damage, PowerUpState powerUpState)
    {
        Rhino rhino = enemy as Rhino;
        if (rhino.isSpecial && powerUpState != PowerUpState.InstantKill)
        {
            agent.enabled = false;
            Instantiate(teleportAnimation, objectToDestroy.transform.position + new Vector3(0,1,0), objectToDestroy.transform.rotation);
            objectToDestroy.transform.position = teleportLocations[rhino.indexLocation].transform.position;
            teleportLocations[rhino.indexLocation].SetActive(true);
            agent.enabled = true;
            agent.SetDestination(target.position);
            StartAgent();
            rhino.isSpecial = false;
        }
        else
        {
            base.TakeDamage(damage, powerUpState);
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("idle") ||
                animator.GetCurrentAnimatorStateInfo(0).IsName("walk"))
            {
                animator.SetBool("Damaged", true);
            }
        }
    }

    public void StartAgent()
    {
        shouldAttack = false;
        agent.speed = 5;
        agent.isStopped = false;
    }
}
