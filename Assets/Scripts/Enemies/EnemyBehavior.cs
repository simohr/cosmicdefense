﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehavior : MonoBehaviour {

    public Enemies enemyType;
    public GameObject objectToDestroy;
    public Collider physicsCollider;
    public GameObject triggerColliders;
    public Transform target;
    public NavMeshAgent agent;
    public GvrAudioSource hitSound;
    public GvrAudioSource getHitSound;

    public Enemy enemy;
    [HideInInspector]
    public float difficultyIncrease;
    protected Animator animator;

    protected bool shouldAttack = false;
    private float destroyTime = 2f;
    private float time = 0f;
    private bool isAlive = true;

    private float secondsPerAttack = 2f;
    private float hitTime;

    private bool slowed = false;
    private float slowDuration = 4f;
    private float slowTime;

    protected void Start()
    {
        agent.SetDestination(target.position);
        animator = GetComponentInParent<Animator>();
        switch (enemyType)
        {
            case Enemies.Golem:
                enemy = new Enemy(70 * difficultyIncrease, 30 * difficultyIncrease, 25, (int)(45 * difficultyIncrease), 1.5f);
                break;
            case Enemies.Rhino:
                enemy = new Rhino(50 * difficultyIncrease, 10 * difficultyIncrease, 0, (int)(50 * difficultyIncrease), 3);
                break;
        }
        agent.speed = enemy.Speed;
    }

    protected void Update ()
    {
        animator.SetFloat("Speed", agent.speed);

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("hit"))
        {
            animator.SetBool("Hitting", false);
        }
        else if (animator.GetCurrentAnimatorStateInfo(0).IsName("die") && isAlive)
        {
            triggerColliders.SetActive(false);
            animator.SetBool("Dead", false);
            time = Time.time + destroyTime;
            isAlive = false;
        }

        if (Time.time > slowTime && !shouldAttack)
        {
            agent.speed = enemy.Speed;
            slowed = false;
        }

        Hit();
        DeathAndDestruction();
    }

    public void StopAgentAndAttackTarget()
    {
        if (!agent.isStopped)
        {
            shouldAttack = true;
            agent.isStopped = true;
            agent.speed = 0;
        }
    }

    public void DeathAndDestruction()
    {
        if (!enemy.isAlive)
        {
            agent.isStopped = true;
            animator.SetBool("Dead", true);
        }

        if (!isAlive && Time.time > time)
        {
            Destroy(objectToDestroy);
        }
    }


    public void Hit()
    {
        if (shouldAttack && Time.time > hitTime)
        {
            hitTime = Time.time + secondsPerAttack;
            target.GetComponent<PlayerTarget>().TakeDamage(enemy.Damage);
            animator.SetBool("Hitting", true);
            hitSound.Play();
        }
    }

    public virtual void TakeDamage(float damage, PowerUpState powerUpState)
    {
        enemy.TakeDamage(damage);
        getHitSound.Play();
        if (powerUpState == PowerUpState.Slowing && !shouldAttack)
        {
            if (slowed)
            {
                slowTime = Time.time + Time.deltaTime + slowDuration;
            }
            else
            { 
                slowed = true;
                agent.speed = agent.speed / 2;
                slowTime = Time.time + Time.deltaTime + slowDuration;
            }
        }
    }
}
