﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemBehaviour : EnemyBehavior
{

	void Start ()
    {
        base.Start();
	}
	
	void Update ()
    {
        base.Update();
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("damage"))
        {
            animator.SetBool("Damaged", false);
        }
    }

    public override void TakeDamage(float damage, PowerUpState powerUpState)
    {
        base.TakeDamage(damage, powerUpState);
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("idle") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("walk"))
        {
            animator.SetBool("Damaged", true);
        }
    }
}
