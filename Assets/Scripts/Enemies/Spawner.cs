﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public List<GameObject> enemiesToSpawn;
    public List<GameObject> teleportLocations;
    public Transform target;
    public Transform parent;

    [HideInInspector]
    public float difficultyIncrease;

    public void SpawnEnemy()
    {
        int randomEnemy = Random.Range(0, enemiesToSpawn.Count);
        GameObject newEnemy = Instantiate(enemiesToSpawn[randomEnemy],transform.position,transform.rotation);
        newEnemy.GetComponentInChildren<EnemyBehavior>().target = target;
        newEnemy.GetComponentInChildren<EnemyBehavior>().difficultyIncrease = difficultyIncrease;
        newEnemy.transform.parent = parent;
        if (newEnemy.GetComponentInChildren<EnemyBehavior>().enemyType == Enemies.Rhino)
        {
            newEnemy.GetComponentInChildren<RhinoBehavior>().teleportLocations = teleportLocations;
        }
        
    }
}
