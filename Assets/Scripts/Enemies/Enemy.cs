﻿using UnityEngine;

public enum Enemies
{
    Golem,
    Rhino,
    Alien
}

public class Enemy
{
    private float healthRemaining;
    private float damage;
    private float armor;
    private int points;
    private float speed;

    [HideInInspector]
    public bool isAlive;
    [HideInInspector]
    public float HealthRemaing
    {
        get
        {
            return healthRemaining;
        }
        private set
        {
            this.healthRemaining = value;
        }
    }

    [HideInInspector]
    public float Damage
    {
        get
        {
            return damage;
        }
        private set
        {
            this.damage = value;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }
        private set
        {
            this.speed = value;
        }
    }


    public Enemy(float maxHP, float damage, float armor, int points, float speed)
    {
        healthRemaining = maxHP;
        this.damage = damage;
        this.armor = armor;
        this.points = points;
        this.Speed = speed;
        isAlive = true;
    }

    public virtual void TakeDamage(float hitDamage)
    {
        float reduction = hitDamage * (armor / 100);
        hitDamage = hitDamage - reduction;
        healthRemaining = healthRemaining - hitDamage;
        if (healthRemaining <= 0)
        {
            isAlive = false;
            DifficultyManager.Instance.score += points;
        }
    }

    public void Hit(PlayerTarget player)
    {
        player.TakeDamage(damage);
    }

}
