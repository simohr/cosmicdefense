﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyBehavior : MonoBehaviour {

    public Transform path;
    public float speed;
    public Transform target;
    [HideInInspector]
    public Enemy enemy;
    public Transform barrel;
    public GameObject triggerColliders;
    public GvrAudioSource hitSound;
    [HideInInspector]
    public float difficultyIncrease;

    private List<Transform> pathPoints;
    private int current;
    private bool aiming = false;
    private Animator animator;

    private float time;
    private float secondsPerAttack = 3f;
    private LineRenderer laserLine;
    private bool isAlive = true;

    void Start ()
    {
        laserLine = GetComponent<LineRenderer>();
        enemy = new Enemy(50 * difficultyIncrease, 20 * difficultyIncrease, 0, (int)(55 * difficultyIncrease), 10);
        animator = GetComponent<Animator>();
        animator.SetBool("Flying", true);
        pathPoints = new List<Transform>();
        foreach (Transform item in path)
        {
            pathPoints.Add(item);
        }
	}

    void Update ()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("damaged"))
        {
            animator.SetBool("Damaged", false);
        }
        else if (animator.GetCurrentAnimatorStateInfo(0).IsName("shoot"))
        {
            animator.SetBool("Shoot", false);
        }
        else if (animator.GetCurrentAnimatorStateInfo(0).IsName("dead") && isAlive)
        {
            triggerColliders.SetActive(false);
            animator.SetBool("Dead", false);
            time = Time.time + 4f;
            isAlive = false;
        }

        if (!aiming)
        {
            if (transform.position != pathPoints[current].position && enemy.isAlive)
            {
                Vector3 pos = Vector3.MoveTowards(transform.position, pathPoints[current].position, speed * Time.deltaTime);
                GetComponent<Rigidbody>().MovePosition(pos);
            }
            else
            {
                if (current != pathPoints.Count - 1)
                {
                    current = current + 1;
                }
                else
                {
                    animator.SetBool("Aiming", true);
                    animator.SetBool("Flying", false);
                    aiming = true;
                    time = Time.time + Time.deltaTime + secondsPerAttack;
                }
            }
        }
        else
        {
            StartShooting();
        }

        DeathAndDestruction();
        RotateTowardsTarget();
	}

    public void StartShooting()
    {
        if (Time.time > time && enemy.isAlive)
        {
            time = Time.time + Time.deltaTime + secondsPerAttack;
            StartCoroutine(ShotEffect());
            GetComponent<GvrAudioSource>().Play();
            animator.SetBool("Shoot", true);
            laserLine.SetPosition(0, barrel.position);
            laserLine.SetPosition(1, target.position);
            hitSound.Play();
            target.GetComponentInParent<PlayerTarget>().TakeDamage(enemy.Damage);
        }
    }

    private IEnumerator ShotEffect()
    {
        laserLine.enabled = true;
        yield return new WaitForSeconds(0.07f);
        laserLine.enabled = false;
    }

    public void RotateTowardsTarget()
    {
        Vector3 targetDir = target.position - transform.position;
        float step = speed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
        Debug.DrawRay(transform.position, newDir, Color.red);
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    public void TakeDamage(float damage)
    {
        enemy.TakeDamage(damage);
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("idle") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("flying") || 
            animator.GetCurrentAnimatorStateInfo(0).IsName("aiming"))
        {
            animator.SetBool("Damaged", true);
        }
    }

    public void DeathAndDestruction()
    {
        if (!enemy.isAlive)
        {
            animator.SetBool("Dead", true);
            GetComponent<Rigidbody>().useGravity = true;
        }

        if (!isAlive && Time.time > time)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Map")
        {
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

}
