﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingSpawner : MonoBehaviour {

    public GameObject enemiesToSpawn;
    public Transform path;
    public Transform target;
    public Transform parent;

    [HideInInspector]
    public float difficultyIncrease;

    public void SpawnEnemy()
    {
        GameObject newEnemy = Instantiate(enemiesToSpawn, transform.position, transform.rotation);
        newEnemy.GetComponentInChildren<FlyingEnemyBehavior>().target = target;
        newEnemy.GetComponentInChildren<FlyingEnemyBehavior>().path = path;
        newEnemy.GetComponentInChildren<FlyingEnemyBehavior>().difficultyIncrease = difficultyIncrease;
        newEnemy.transform.parent = parent;
    }
}
