﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportAnimation : MonoBehaviour {

    private float timeToDeactivate = 1f;
    private float time;
	// Use this for initialization
	void OnEnable ()
    {
        time = Time.time + Time.deltaTime + timeToDeactivate;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (time < Time.time)
        {
            gameObject.SetActive(false);
        }
	}
}
