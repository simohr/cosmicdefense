﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUpState
{
    None,
    Slowing,
    InstantKill
}

public class PlayerTarget : MonoBehaviour {

    private const float PLAYER_DAMAGE = 100f;

    public GameObject gameOverScreen;

    [HideInInspector]
    public float maxHealth = 1000;
    [HideInInspector]
    public float remainingHealth = 1000;
    [HideInInspector]
    public float weaponDamage = PLAYER_DAMAGE;

    [HideInInspector]
    public PowerUpState powerUpState = PowerUpState.None;

    [HideInInspector]
    public float PowerUpTime
    {
        get
        {
            return powerUpTime;
        }
    }

    [HideInInspector]
    public float PowerUpDuration
    {
        get
        {
            return powerUpDuration;
        }
    }

    public bool isAlive;

    private float powerUpDuration = 15f;
    private float powerUpTime;

    public PlayerTarget()
    {
        maxHealth = 1000;
        remainingHealth = 1000;
        isAlive = true;
    }

    public void TakeDamage(float damage)
    {
        remainingHealth -= damage;
        if (remainingHealth <= 0)
        {
            isAlive = false;
           gameOverScreen.SetActive(true);
        }
    }

    public void ActivateInstantKillPU()
    {
        powerUpState = PowerUpState.InstantKill;
        powerUpTime = Time.time + Time.deltaTime + powerUpDuration;
        weaponDamage = 100000f;
    }

    public void DeactivatePowerUp()
    {
        powerUpState = PowerUpState.None;
        weaponDamage = PLAYER_DAMAGE;
    }

    public void ActivateSlowingPU()
    {
        powerUpState = PowerUpState.Slowing;
        powerUpTime = Time.time + Time.deltaTime + powerUpDuration;
    }

    private void Update()
    {
        if (Time.time > powerUpTime)
        {
            DeactivatePowerUp();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GeneralEnemy")
        {
            other.GetComponentInChildren<EnemyBehavior>().StopAgentAndAttackTarget();
        }
    }
}
