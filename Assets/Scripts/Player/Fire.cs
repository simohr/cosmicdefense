﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fire : MonoBehaviour
{

    public PlayerTarget player;
    public float fireRate = 0.5f;
    public float weaponRange = 150f;
    public Camera playerCamera;

    private LineRenderer laserLine;
    private float fireTime = 0f;
    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);

    private float magnetTimer = 2f;
    private float magnetTime;

    private bool setTimer = false;

    private void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        CardboardMagnetSensor.SetEnabled(true);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && player.isAlive)
        {
            FireShot();
        }
        else if (CardboardMagnetSensor.CheckIfWasClicked() && player.isAlive)
        {
            FireShot();
            if (MySceneManager.Instance.inMenu)
            {
                CardboardMagnetSensor.ResetClick();
            }
            if (Time.time > magnetTime && !setTimer)
            {
                magnetTime = Time.time + Time.deltaTime + magnetTimer;
                setTimer = true;
            }
        }

        if (Time.time > magnetTime && !MySceneManager.Instance.inMenu)
        {
            CardboardMagnetSensor.ResetClick();
            setTimer = false;
        }


    }

    public void FireShot()
    {
        if (Time.time > fireTime)
        {
            GetComponent<GvrAudioSource>().Play();
            fireTime = Time.time + fireRate;
            StartCoroutine(ShotEffect());
            Vector3 rayOrigin = playerCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            laserLine.SetPosition(0, transform.position);
            if (Physics.Raycast(rayOrigin, playerCamera.transform.forward, out hit, weaponRange))
            {
                laserLine.SetPosition(1, hit.point);

                if (hit.transform.tag == "EnemyHead")
                {
                    hit.transform.GetComponentInParent<EnemyBehavior>().TakeDamage(player.weaponDamage * 2, player.powerUpState);
                }
                else if (hit.transform.tag == "Enemy")
                {
                    hit.transform.GetComponentInParent<EnemyBehavior>().TakeDamage(player.weaponDamage, player.powerUpState);
                }
                else if (hit.transform.tag == "FlyingEnemy")
                {
                    hit.transform.GetComponentInParent<FlyingEnemyBehavior>().TakeDamage(player.weaponDamage);
                }
                else if (hit.transform.tag == "StartGame")
                {
                    hit.transform.GetComponentInParent<StartTrigger>().StartGame();
                }
            }
            else
            {
                laserLine.SetPosition(1, rayOrigin + playerCamera.transform.forward * weaponRange);
            }

        }
    }

    private IEnumerator ShotEffect()
    {
        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }
}
