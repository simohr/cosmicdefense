﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class MagnetButton : MonoBehaviour, IGvrPointerHoverHandler {
    public UnityEvent onClick;

    private void Awake()
    {
        if (onClick == null)
        {
            onClick = new UnityEvent();
        }
    }


    public void OnGvrPointerHover(PointerEventData eventData)
    {
        if (CardboardMagnetSensor.CheckIfWasClicked())
        {
            CardboardMagnetSensor.ResetClick();
            onClick.Invoke();
        }
    }
}
