﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCanvas : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject difficultyPanel;
    public PlayerTarget player;
    private void OnEnable()
    {
        MySceneManager.Instance.inMenu = true;
        if (MySceneManager.Instance.sceneState == SceneState.GameOver)
        {
            gameOverPanel.SetActive(true);
            difficultyPanel.SetActive(false);
            player.remainingHealth = 1000;
            player.isAlive = true;
        }
    }
}
