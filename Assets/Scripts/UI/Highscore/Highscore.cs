﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerHighscore : IComparable<PlayerHighscore>
{
    public string name;
    public float score;

    public PlayerHighscore(string name, float score)
    {
        this.name = name;
        this.score = score;
    }

    public int CompareTo(PlayerHighscore other)
    {
        return score.CompareTo(other.score);
    }
}

public class Highscore : Singleton<Highscore>
{
    public List<PlayerHighscore> easyHighscores;
    public List<PlayerHighscore> mediumHighscores;
    public List<PlayerHighscore> hardHighscores;

    public void Init()
    {
        LoadData();
    }

    public void FakeData()
    {
        AddHighscore("SimoE", 100, Difficulty.Easy);
        AddHighscore("SimoM", 100, Difficulty.Medium);
        AddHighscore("SimoH", 100, Difficulty.Hard);
    }

    public void AddHighscore(string playerName, float highscore, Difficulty difficulty)
    {
        PlayerHighscore newHighscore = new PlayerHighscore(playerName, highscore);

        switch (difficulty)
        {
            case Difficulty.Easy:
                easyHighscores.Add(newHighscore);
                easyHighscores.Sort();
                easyHighscores.Reverse();
                break;
            case Difficulty.Medium:
                mediumHighscores.Add(newHighscore);
                mediumHighscores.Sort();
                mediumHighscores.Reverse();
                break;
            case Difficulty.Hard:
                hardHighscores.Add(newHighscore);
                hardHighscores.Sort();
                hardHighscores.Reverse();
                break;
        }

        SetData();
    }

    private void SetData()
    {
        PlayerPrefs.SetString("EasyHighscoreData", JsonHelper.ToJsonList(easyHighscores));
        PlayerPrefs.SetString("MediumHighscoreData", JsonHelper.ToJsonList(mediumHighscores));
        PlayerPrefs.SetString("HardHighscoreData", JsonHelper.ToJsonList(hardHighscores));
    }

    private void LoadData()
    {
        easyHighscores = new List<PlayerHighscore>();
        mediumHighscores = new List<PlayerHighscore>();
        hardHighscores = new List<PlayerHighscore>();

        string easyHighscoresData = PlayerPrefs.GetString("EasyHighscoreData", "noData");
        string mediumHighscoresData = PlayerPrefs.GetString("MediumHighscoreData", "noData");
        string hardHighscoresData = PlayerPrefs.GetString("HardHighscoreData", "noData");

        if (easyHighscoresData != "noData")
        {
            easyHighscores = JsonHelper.FromJsonList<PlayerHighscore>(easyHighscoresData);
        }

        if (mediumHighscoresData != "noData")
        {
            mediumHighscores = JsonHelper.FromJsonList<PlayerHighscore>(mediumHighscoresData);
        }

        if (hardHighscoresData != "noData")
        {
            hardHighscores = JsonHelper.FromJsonList<PlayerHighscore>(hardHighscoresData);
        }

        easyHighscores.Sort();
        easyHighscores.Reverse();
        mediumHighscores.Sort();
        mediumHighscores.Reverse();
        hardHighscores.Sort();
        hardHighscores.Reverse();
    }
}
