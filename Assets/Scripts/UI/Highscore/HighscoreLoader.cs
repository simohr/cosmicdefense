﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighscoreLoader : MonoBehaviour
{
    public GameObject highscorePanel;
    public GameObject mainMenuPanel;
    public GameObject highscoreItemPrefab;

    public Transform easyPanel;
    public Transform mediumPanel;
    public Transform hardPanel;


    // Use this for initialization
    void OnEnable ()
    {

        foreach (Transform item in easyPanel)
        {
            Destroy(item.gameObject);
        }

        foreach (Transform item in mediumPanel)
        {
            Destroy(item.gameObject);
        }

        foreach (Transform item in hardPanel)
        {
            Destroy(item.gameObject);
        }

        for (int i=0; i< Highscore.Instance.easyHighscores.Count; i++)
        {
            if (i >= 5)
                continue;

            GameObject newItem = Instantiate(highscoreItemPrefab, easyPanel, false);
            HighscoreItem script = newItem.GetComponent<HighscoreItem>();
            script.number.text = (i + 1).ToString();
            script.playerName.text = Highscore.Instance.easyHighscores[i].name;
            script.score.text = Highscore.Instance.easyHighscores[i].score.ToString();
        }

        for (int i = 0; i < Highscore.Instance.mediumHighscores.Count; i++)
        {
            if (i >= 5)
                continue;

            GameObject newItem = Instantiate(highscoreItemPrefab, mediumPanel, false);
            HighscoreItem script = newItem.GetComponent<HighscoreItem>();
            script.number.text = (i + 1).ToString();
            script.playerName.text = Highscore.Instance.mediumHighscores[i].name;
            script.score.text = Highscore.Instance.mediumHighscores[i].score.ToString();
        }

        for (int i = 0; i < Highscore.Instance.hardHighscores.Count; i++)
        {
            if (i >= 5)
                continue;

            GameObject newItem = Instantiate(highscoreItemPrefab, hardPanel, false);
            HighscoreItem script = newItem.GetComponent<HighscoreItem>();
            script.number.text = (i + 1).ToString();
            script.playerName.text = Highscore.Instance.hardHighscores[i].name;
            script.score.text = Highscore.Instance.hardHighscores[i].score.ToString();
        }
    }
	

    public void OnBackClick()
    {
        highscorePanel.SetActive(false);
        mainMenuPanel.SetActive(true);
    }
}
