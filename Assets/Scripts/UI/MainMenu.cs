﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenuPanel;
    public GameObject creditsPanel;
    public GameObject highscorePanel;
    public GameObject difficultyPanel;
    public GameObject gameOverPanel;


    public void OnStartClick()
    {
        difficultyPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void OnHighscoreClick()
    {
        highscorePanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void OnRulesClick()
    {
        creditsPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void OnRulesBackClick()
    {
        creditsPanel.SetActive(false);
        mainMenuPanel.SetActive(true);
    }

    public void OnDifficultyBackClick()
    {
        difficultyPanel.SetActive(false);
        mainMenuPanel.SetActive(true);
    }

    public void OnExitClick()
    {
        Application.Quit();
    }


    private void Start()
    {
        Highscore.Instance.Init();
    }
}
