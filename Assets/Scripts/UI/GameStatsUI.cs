﻿using UnityEngine;
using UnityEngine.UI;

public class GameStatsUI : MonoBehaviour {

    public Text round;
    public Text roundTime;
    public Text afterRoundTime;
    public Text points;
	
	// Update is called once per frame
	void Update ()
    {
        points.text = DifficultyManager.Instance.score.ToString();
	}
}
