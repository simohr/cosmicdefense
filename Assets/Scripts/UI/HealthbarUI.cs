﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarUI : MonoBehaviour {

    public Image healthBar;
    public Text healthPercentige;
	
	// Update is called once per frame
	void Update ()
    {
        healthBar.fillAmount = GetComponent<PlayerTarget>().remainingHealth / GetComponent<PlayerTarget>().maxHealth;
        healthPercentige.text = ((GetComponent<PlayerTarget>().remainingHealth / GetComponent<PlayerTarget>().maxHealth) * 100).ToString() + "%";

    }
}
