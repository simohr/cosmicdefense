﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpsUI : MonoBehaviour {

    public PlayerTarget player;
    public Text healingPUCountText;
    public Text instantKillPUCountText;
    public Text slowingPUCountText;
    public Image powerUpFrame;
    public Text powerUpTimeRemainingText;

    private int healingPUCount = 0;
    private int instantKillPUCount = 0;
    private int slowingPUCount = 0;

    public void InitializePowerUps()
    {
        switch (DifficultyManager.Instance.difficulty)
        {
            case Difficulty.Easy:
                healingPUCount = 2;
                instantKillPUCount = 2;
                slowingPUCount = 2;
                break;
            case Difficulty.Medium:
                healingPUCount = 1;
                instantKillPUCount = 1;
                slowingPUCount = 1;
                break;
            case Difficulty.Hard:
                healingPUCount = 0;
                instantKillPUCount = 0;
                slowingPUCount = 0;
                break;
        }
        UpdateUI();
    }

    public void OnHealingPUPressed()
    {
        if (healingPUCount > 0)
        {
            player.remainingHealth += player.maxHealth * 0.25f;
            if (player.remainingHealth > player.maxHealth)
            {
                player.remainingHealth = player.maxHealth;
            }
            healingPUCount--;
            UpdateUI();
        }
    }

    public void OnInstaKillPUPressed()
    {
        if (instantKillPUCount > 0)
        {
            player.ActivateInstantKillPU();
            instantKillPUCount--;
            UpdateUI();
        }
    }

    public void OnSlowingPUPressed()
    {
        if (slowingPUCount > 0)
        {
            player.ActivateSlowingPU();
            slowingPUCount--;
            UpdateUI();
        }
    }

    public void GiveRandomPowerUp()
    {
        int randomNumber = Random.Range(1, 101);

        if (randomNumber > 0 && randomNumber <= 10)
        {
            instantKillPUCount++;
        }
        else if (randomNumber > 10 && randomNumber <= 40)
        {
            healingPUCount++;
        }
        else if (randomNumber > 40 && randomNumber <= 80)
        {
            slowingPUCount++;
        }
        UpdateUI();
    }

    private void UpdateUI()
    {
        healingPUCountText.text = "x" + healingPUCount.ToString();
        instantKillPUCountText.text = "x" + instantKillPUCount.ToString();
        slowingPUCountText.text = "x" + slowingPUCount.ToString();
    }

    private void UpdatePowerUpFrame()
    {
        powerUpFrame.fillAmount = (player.PowerUpTime - Time.time) / player.PowerUpDuration;
        powerUpTimeRemainingText.text = ((int)(player.PowerUpTime - Time.time)).ToString();
    }

    private void Update()
    {
        if (player.powerUpState != PowerUpState.None)
        {
            UpdatePowerUpFrame();
        }
        else
        {
            powerUpFrame.fillAmount = 0;
            powerUpTimeRemainingText.text = "0";
        }
    }
}
