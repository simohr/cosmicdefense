﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficutlyMenu : MonoBehaviour {

    public GameObject game;
    public GameObject menu;

    public void OnEasyClick()
    {
        DifficultyManager.Instance.difficulty = Difficulty.Easy;
        DifficultyManager.Instance.score = 0;
        game.SetActive(true);
        menu.SetActive(false);
        MySceneManager.Instance.inMenu = false;
    }

    public void OnMediumClick()
    {
        DifficultyManager.Instance.difficulty = Difficulty.Medium;
        DifficultyManager.Instance.score = 0;
        game.SetActive(true);
        menu.SetActive(false);
        MySceneManager.Instance.inMenu = false;
    }

    public void OnHardClick()
    {
        DifficultyManager.Instance.difficulty = Difficulty.Hard;
        DifficultyManager.Instance.score = 0;
        game.SetActive(true);
        menu.SetActive(false);
        MySceneManager.Instance.inMenu = false;
    }
}
