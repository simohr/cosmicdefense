﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    public GameObject highScorePanel;
    public GameObject gameOverPanel;
    public Text score;
    public Text difficulty;

	// Use this for initialization
	void OnEnable ()
    {
        score.text = DifficultyManager.Instance.score.ToString();
        difficulty.text = DifficultyManager.Instance.difficulty.ToString();
	}
	

    public void OnContinueClick()
    {
        MySceneManager.Instance.sceneState = SceneState.MainMenu;

        Highscore.Instance.AddHighscore("Score", float.Parse(score.text), DifficultyManager.Instance.difficulty);
        highScorePanel.SetActive(true);
        gameOverPanel.SetActive(false);

    }
}
