﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

    public GameObject panel;
    public GameObject game;
    public GameObject menu;
    public Transform enemies;
    public GameObject spawner;
    public GameObject startTrigger;
	// Use this for initialization
	void OnEnable ()
    {
        MySceneManager.Instance.sceneState = SceneState.GameOver;
        foreach (Transform item in enemies)
        {
            Destroy(item.gameObject);
        }

        spawner.SetActive(false);

        EndGame();
	}

    private void EndGame()
    {
        menu.SetActive(true);
        game.SetActive(false);
        gameObject.SetActive(false);
        startTrigger.SetActive(true);

    }
}
