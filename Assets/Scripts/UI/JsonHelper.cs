﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;


public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    public static List<T> FromJsonList<T>(string json)
    {
        ListWrapper<T> wrapper = JsonUtility.FromJson<ListWrapper<T>>(json);
        return wrapper.ListItems;
    }

    public static string ToJsonList<T>(List<T> list)
    {
        ListWrapper<T> wrapper = new ListWrapper<T>();
        wrapper.ListItems = list;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJsonList<T>(List<T> list, bool prettyPrint)
    {
        ListWrapper<T> wrapper = new ListWrapper<T>();
        wrapper.ListItems = list;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }

    [Serializable]
    private class ListWrapper<T>
    {
        public List<T> ListItems;
    }
}
