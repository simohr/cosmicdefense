﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTrigger : MonoBehaviour
{
    public GameObject spawnerManager;

    public void StartGame()
    {
        spawnerManager.SetActive(true);
        gameObject.SetActive(false);
    }

}
